create table IF NOT EXISTS article
(
    id              int auto_increment
    primary key,
    created_by      varchar(255) null,
    created_date    bigint(15)   null,
    modified_date   bigint(15)   null,
    modified_by     varchar(255) null,
    secure_id       varchar(50)  null,
    author          text null,
    title           text null,
    body            text null,
    );