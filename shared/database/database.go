package database

import (
	"fmt"
	"gitlab.com/kaes25/shared/config"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

type Database struct {
	db *gorm.DB
}

func SetupDatabase(config *config.Config) *Database {
	fmt.Println("Try Setup Database ...")

	url := fmt.Sprintf(config.DB.Datasource)
	db, err := gorm.Open(postgres.Open(url), &gorm.Config{
		DisableAutomaticPing: false,
	})

	if err != nil {
		panic(err)
	}

	return &Database{db}
}

func (d *Database) GetDB() *gorm.DB {
	return d.db
}
