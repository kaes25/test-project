package config

import (
	"fmt"
	"github.com/spf13/viper"
	"os"
	"path/filepath"
	"time"
)

type Config struct {
	App          AppConfig          `mapstructure:"app" json:"app" yaml:"app"`
	DB           DatabaseConfig     `mapstructure:"db" json:"db" yaml:"db"`
	Otp          OtpConfig          `mapstructure:"otp" json:"otp" yaml:"otp"`
	Notification NotificationConfig `mapstructure:"notification" json:"notification"`
}

type AppConfig struct {
	HttpsEnabled       bool   `mapstructure:"httpsenabled" json:"https_enabled" ymal:"httpsenabled"`
	Version            string `mapstructure:"version" json:"version" ymal:"version"`
	Addr               string `mapstructure:"addr" json:"addr" ymal:"addr"`
	Mode               string `mapstructure:"mode" json:"mode" ymal:"mode"`
	MaxMultipartMemory int    `mapstructure:"max_multipart_memory" json:"max_multipart_memory" ymal:"max_multipart_memory"`
	BaseURL            string `mapstructure:"base_url" yaml:"base_url"`
	ViewPath           string `mapstructure:"view_path" yaml:"view_path"`
	EmailTemplates     string `mapstructure:"email_templates" yaml:"email_templates"`
	SupportPhone       string `mapstructure:"support_phone" yaml:"support_phone"`
	SupportEmail       string `mapstructure:"support_email" yaml:"support_email"`
	ServiceName        string `mapstructure:"service_name" json:"service_name"`
	RSAKEY             string `mapstructure:"rsa_key" json:"rsa_key"`
}

// OtpConfig is the OTP Config
type OtpConfig struct {
	ExpiryTime string `mapstructure:"expiry_time" yaml:"expiry_time"`
	Retries    int    `mapstructure:"retries" yaml:"retries"`
	Email      string `mapstructure:"email" yaml:"email"`
	Pass       string `mapstructure:"pass" yaml:"pass"`
}

type DatabaseConfig struct {
	Datasource   string `mapstructure:"datasource" yaml:"datasource" json:"datasource"`
	MaxIdleConns int    `mapstructure:"max_idle_conns" yaml:"max_idle_conns" json:"max_idle_conns"`
	MaxOpenConns int    `mapstructure:"max_open_conns" yaml:"max_open_conns" json:"max_open_conns"`
}

// NotificationConfig to specify micro-service application configuration
type NotificationConfig struct {
	Username         string        `mapstructure:"username" yaml:"username"`
	Password         string        `mapstructure:"password" yaml:"password"`
	Timeout          time.Duration `mapstructure:"timeout" yaml:"timeout"`
	SmsUrl           string        `mapstructure:"sms_url" yaml:"sms_url"`
	EmailUrl         string        `mapstructure:"email_url" yaml:"email_url"`
	EmailHeader      string        `mapstructure:"email_header" yaml:"email_header"`
	RmDashboardUrl   string        `mapstructure:"rm_dashboard_url" yaml:"rm_dashboard_url"`
	ResetPasswordUrl string        `mapstructure:"reset_password_url" yaml:"reset_password_url"`
	LimitLink        string        `json:"limit_link" yaml:"limit_link"`
	TokenResetTime   string        `mapstructure:"token_reset_time" yaml:"token_reset_time"`
}

var (
	App          AppConfig
	DB           DatabaseConfig
	Otp          OtpConfig
	Notification NotificationConfig
)

func SetupConfig() (config *Config) {
	fmt.Println("Setup Config ....")
	config = &Config{}

	viper.SetConfigName("config.yml") // name of config file (without extension)
	viper.SetConfigType("yaml")       // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath("./resource/")
	err := viper.ReadInConfig()
	if err != nil {
		readJsonConfig()
	}

	err = viper.Unmarshal(config)
	if err != nil {
		panic(err)
	}
	App = config.App
	DB = config.DB
	Otp = config.Otp
	Notification = config.Notification

	return config
}

func (c *Config) GetHttpAddress() string {
	return c.App.Addr
}

func readJsonConfig() {
	env := os.Getenv("ENV_NAME")
	if env == "PRODUCTION" {
		viper.SetConfigName("config_production.json") // name of config file (without extension)
	} else if env == "ALPHA" {
		viper.SetConfigName("config_alpha.json") // name of config file (without extension)
	} else if env == "STAGING" {
		viper.SetConfigName("config_staging.json") // name of config file (without extension)
	} else if env == "DEVELOPMENT" {
		viper.SetConfigName("config_development.json") // name of config file (without extension)
	} else {
		panic(fmt.Sprintf("Environment name %s not found", env))
	}

	path, _ := os.Getwd()
	viper.SetConfigType("json") // REQUIRED if the config file does not have the extension in the name
	viper.AddConfigPath(filepath.Join(path, "shared", "config_files"))
	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}
