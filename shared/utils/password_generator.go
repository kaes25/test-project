package utils

import (
	"log"

	"golang.org/x/crypto/bcrypt"
)

func MatchPassword(userPassword string, password string) (isValid bool, err error) {
	err = bcrypt.CompareHashAndPassword([]byte(userPassword), []byte(password))
	if err != nil {
		return false, err
	}
	return true, nil
}

func EncryptPassword(in string) (out string, err error) {
	hashedMpin, err := Hash(in)
	if err != nil {
		log.Println("Error whie encrypting mpin", err)
		return "", err
	}
	out = string(hashedMpin)
	return
}

//hash password
func HashPassword(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}
