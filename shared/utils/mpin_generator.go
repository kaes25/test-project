package utils

import (
	"golang.org/x/crypto/bcrypt"
	"log"
)

func EncryptMPIN(in string) (out string, err error) {
	hashedMpin, err := Hash(in)
	if err != nil {
		log.Println("Error whie encrypting mpin", err)
		return "", err
	}
	out = string(hashedMpin)
	return
}

//hash password or MPIN
func Hash(password string) ([]byte, error) {
	return bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
}

//Match User MPIN
func MatchMpin(userPin string, in string) (isValid bool, err error) {
	err = bcrypt.CompareHashAndPassword([]byte(userPin), []byte(in))
	if err != nil {
		return false, err
	}
	return true, nil
}
