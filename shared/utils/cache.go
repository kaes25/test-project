package utils

import (
	"encoding/json"
	"github.com/gofiber/fiber/v2"
	"github.com/jellydator/ttlcache/v2"
	"gitlab.com/kaes25/entity"
	"io/ioutil"
	"net/http"
	"time"
)

var cache ttlcache.SimpleCache = ttlcache.NewCache()

func verifyCache(c *fiber.Ctx) error {
	id := c.Params("id")
	val, err := cache.Get(id)
	if err != ttlcache.ErrNotFound {
		return c.JSON(fiber.Map{"Cached": val})
	}
	return c.Next()
}

func Cache(secureId string) {
	app := fiber.New()

	cache.SetTTL(time.Duration(10 * time.Second))

	app.Get("/:id", verifyCache, func(c *fiber.Ctx) error {
		id := secureId
		res, err := http.Get("https://jsonplaceholder.typicode.com/todos/" + id)
		if err != nil {
			return err
		}

		defer res.Body.Close()
		body, err := ioutil.ReadAll(res.Body)
		if err != nil {
			return err
		}

		todo := entity.Article{}
		parseErr := json.Unmarshal(body, &todo)
		if parseErr != nil {
			return parseErr
		}

		cache.Set(id, todo)
		return c.JSON(fiber.Map{"Data": todo})
	})

}
