package utils

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strconv"

	"github.com/gin-gonic/gin"
)

// ResponseObject Successful response
// swagger:response responseObject
type ResponseObject struct {
	// Message String To Send
	Message string `json:"message"`
	// Data object
	Data interface{} `json:"data"`
}

// ErrorObject Unsuccessful request
// swagger:response errorObject
type ErrorObject struct {
	// The error description message
	ErrorDescription string `json:"error_description"`
	// The error message
	Error string `json:"error"`
}

//Send used to send the response
func SendResponse(ctx *gin.Context, statusCode int, result bool, message string, data interface{}) {
	if result == true {
		ctx.JSON(statusCode, gin.H{"message": message, "data": data})
		return
	}
	ctx.AbortWithStatusJSON(statusCode, gin.H{"error_description": message})
}

//Send used to send the response
func SendWithStatusCode(c *gin.Context, httpStatusCode int, result bool, message string, data interface{}, statusCode int) {
	if result {
		//log.Println("[INFO] - Response ", gin.H{"message": message, "data": data})
		c.JSON(httpStatusCode, gin.H{"message": message, "data": data})
		return
	}
	c.JSON(httpStatusCode, gin.H{"error_description": message, "statusCode": statusCode})
}

//Send used to send the response
func SendWithPagination(c *gin.Context, statusCode int, result bool, message string, data interface{}, pagination Pagination) {
	if result {
		paginationResponse := map[string]interface{}{
			"total_data": pagination.TotalData,
			"total_page": pagination.TotalPage,
		}
		//log.Println("[INFO] - Response ", gin.H{"message": message, "data": data, "pagination": paginationResponse})
		c.JSON(statusCode, gin.H{"message": message, "data": data, "pagination": paginationResponse})
		return
	}
	c.JSON(statusCode, gin.H{"error_description": message})
}

//Send used to send the response
func SendWithPaginationWithPageLimit(c *gin.Context, statusCode int, result bool, message string, data interface{}, pagination Pagination) {
	if result {
		paginationResponse := map[string]interface{}{
			"totalData": pagination.TotalData,
			"totalPage": pagination.TotalPage,
			"page":      pagination.Page + 1,
			"limit":     pagination.Limit,
		}
		//log.Println("[INFO] - Response ", gin.H{"message": message, "data": data, "pagination": paginationResponse})
		c.JSON(statusCode, gin.H{"message": message, "data": data, "pagination": paginationResponse})
		return
	}
	c.JSON(statusCode, gin.H{"error_description": message})
}

func JSON(w http.ResponseWriter, statusCode int, data interface{}) {
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	err := json.NewEncoder(w).Encode(data)
	if err != nil {
		fmt.Fprintf(w, "%s", err.Error())
	}
}

func ERROR(w http.ResponseWriter, statusCode int, err error) {
	if err != nil {
		JSON(w, statusCode, struct {
			Error string `json:"error"`
		}{
			Error: err.Error(),
		})
		return
	}
	JSON(w, http.StatusBadRequest, nil)
}

type Pagination struct {
	Page      int    `json:"page"`
	Limit     int    `json:"limit"`
	Offset    int    `json:"offset"`
	Order     string `json:"order"`
	TotalData int    `json:"totalData"`
	TotalPage int    `json:"totalPage"`
}

func (p Pagination) TotalDataWithPageLimit() map[string]int {
	return map[string]int{
		"totalData": p.TotalData,
		"totalPage": p.TotalPage,
		"page":      p.Page + 1,
		"limit":     p.Limit,
	}
}

func NewPagination(queryParam url.Values) (Pagination, error) {
	pagination := Pagination{}
	pagination.Page = 0
	pagination.Limit = 10
	pagination.Order = "id DESC"
	if queryParam.Get("page") != "" {
		page, err := strconv.Atoi(queryParam.Get("page"))
		if err != nil {
			return pagination, errors.New("invalid page parameter")
		}
		pagination.Page = page - 1
	}

	if queryParam.Get("limit") != "" {
		limit, err := strconv.Atoi(queryParam.Get("limit"))
		if err != nil {
			return pagination, errors.New("invalid page parameter")
		}
		pagination.Limit = limit
	}

	pagination.Offset = pagination.Limit * pagination.Page
	return pagination, nil
}

type KpayAccountDetail struct {
	Status  string                `json:"status"`
	Message string                `json:"message"`
	Data    kpayAccountDetailData `json:"data"`
}

type kpayAccountDetailData struct {
	Accounts []kpayAccountDetailAccounts `json:"accounts"`
	Balance  int64                       `json:"balance"`
}

type kpayAccountDetailAccounts struct {
	VaccNo       string `json:"vaccNo"`
	VaccName     string `json:"vaccName"`
	VaccBank     string `json:"vaccBank"`
	VaccBankName string `json:"vaccBankName"`
	VaccStatus   string `json:"vaccStatus"`
	Balance      int64  `json:"balance"`
}

type PrivyAPIResponse struct {
	Code   float64 `json:"code"`
	Errors []struct {
		Field    string   `json:"field"`
		Messages []string `json:"messages"`
	} `json:"errors"`
	Message string       `json:"message"`
	Data    PrivyAPIData `json:"data"`
}

type PrivyAPIData struct {
	UserToken   string `json:"userToken,omitempty"`
	Email       string `json:"email,omitempty"`
	Phone       string `json:"phone,omitempty"`
	Status      string `json:"status,omitempty"`
	DocToken    string `json:"docToken,omitempty"`
	UrlDocument string `json:"urlDocument,omitempty"`
	Recipients  []struct {
		PrivyId         string `json:"privyId,omitempty"`
		Type            string `json:"type,omitempty"`
		EnterpriseToken string `json:"enterpriseToken,omitempty"`
		SignatoryStatus string `json:"signatoryStatus,omitempty"`
	} `json:"recipients,omitempty"`
	Download struct {
		Url       string `json:"url"`
		ExpiredAt string `json:"expiredAt"`
	} `json:"download,omitempty"`
	DocumentStatus string `json:"documentStatus,omitempty"`
}

type PaymentTransferRequest struct {
	ExternalId        string `json:"external_id"`
	SenderClientRef   string `json:"sender_client_ref"`
	ReceiverClientRef string `json:"receiver_client_ref"`
	UserId            string `json:"user_id"`
	Description       string `json:"description"`
	Amount            string `json:"amount"`
	LoanId            int    `json:"loan_id"`
	LoanNumber        string `json:"loan_number"`
	TrxType           int    `json:"trx_type"`
}

func (p Pagination) TotalDataAndPage() map[string]int {
	return map[string]int{
		"totalData": p.TotalData,
		"totalPage": p.TotalPage,
	}
}
