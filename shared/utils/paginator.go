package utils

import (
	"encoding/json"
	"errors"
	"github.com/jinzhu/gorm"
	"math"
)


// AfterFind
type AfterFind func(interface{}) (interface{}, error)

// PaginatorOptions
type PaginatorOptions struct {
	TotalRecord uint        `json:"total_record"`
	TotalPage   uint        `json:"total_page"`
	Records     interface{} `json:"records"`
	Limit       uint        `json:"limit"`
	Page        uint        `json:"page"`
	Result      interface{} `json:"-"`
	Response    interface{} `json:"-"`
	AfterFind   AfterFind   `json:"-"`
}

// Paging map query and result with total records, records and page no of result
func Paging(query *gorm.DB, p *PaginatorOptions) (*PaginatorOptions, error) {
	countRecords := func(db *gorm.DB, anyType interface{}, done chan bool, count *uint) {
		db.Model(anyType).Count(count)
		done <- true
	}
	if p.Page < 1 {
		p.Page = 1
	}
	if p.Limit == 0 {
		p.Limit = 50
	}
	done := make(chan bool, 1)
	var paginator PaginatorOptions
	var count uint
	var offset uint
	go countRecords(query, p.Result, done, &count)

	if p.Page == 1 {
		offset = 0
	} else {
		offset = (p.Page - 1) * p.Limit
	}
	errFind := query.Limit(p.Limit).Offset(offset).Find(p.Result).Error
	<-done
	paginator.TotalPage = uint(math.Ceil(float64(count) / float64(p.Limit)))
	paginator.TotalRecord = count
	paginator.Page = p.Page
	paginator.Limit = p.Limit

	if paginator.TotalPage == 0 {
		return &paginator, errors.New("No results found for the search criteria")
	}
	if paginator.Page > paginator.TotalPage {
		return &paginator, errors.New("Page number is greater than total no. of pages")
	}

	if errFind != nil {
		return &paginator, errFind
	}
	paginator.Records = p.Result
	if p.Response != nil {
		marshalled, errFind := json.Marshal(p.Result)
		if errFind != nil {
			return &paginator, errFind
		}
		errFind = json.Unmarshal(marshalled, &p.Response)
		if errFind != nil {
			return &paginator, errFind
		}
		paginator.Records = p.Response
	}
	if p.AfterFind != nil {
		response, err := p.AfterFind(paginator.Records)
		if err != nil {
			return &paginator, errFind
		}
		paginator.Records = response
	}
	return &paginator, nil
}

