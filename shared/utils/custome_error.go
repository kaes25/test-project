package utils

import (
	"errors"
)

// Model errors
var (
	ErrInvalidArgs  = errors.New("Invalid Args")
	ErrKeyConflict  = errors.New("Key Conflict")
	ErrDataNotFound = errors.New("Record Not Found")
	ErrUserExists   = errors.New("User already exists")
	ErrUnknown      = errors.New("Unknown Error")
	ErrFailed       = errors.New("Failed")
	ErrOtpInvalid   = errors.New("Kode yang anda masukkan salah")
	ErrInvalidName  = errors.New("Nama nasabah tidak sesuai dengan nama KTP")
)

var (
	ERR_BAD_REQUEST          = "bad_request"
	ERR_UNPROCESSABLE_ENTITY = "unprocessable_entity"
)

