package utils

func IsValidReligion(religion string) bool {
	switch religion {
	case
		"islam",
		"kristen",
		"katolik",
		"budha",
		"hindu",
		"kong hu chu",
		"lain-lain":
		return true
	}
	return false
}

func IsValidNationality(nationality string) bool {
	switch nationality {
	case
		"WNI",
		"WNA":
		return true
	}
	return false
}

func IsValidEmailStatus(emailStatus uint8) bool {
	switch emailStatus {
	case
		0,
		1,
		2:
		return true
	}
	return false
}
