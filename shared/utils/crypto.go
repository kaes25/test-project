package utils

import (
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"math/rand"
	"time"
)

type HashedPassword struct {
	Salt   string
	Pwd    string
	Pepper string
}

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")

//Generate randstring of size n
func RandString(data []rune, n int) string {
	s1 := rand.NewSource(time.Now().UnixNano())
	r1 := rand.New(s1)

	b := make([]rune, n)
	for i := range b {
		b[i] = data[r1.Intn(len(data))]
	}
	return string(b)
}

func NewGenerateHashPassword(pwd string) (resp HashedPassword) {
	resp.Salt = RandString(letters, 10)
	time.Sleep(time.Nanosecond)
	resp.Pepper = RandString(letters, 10)
	resp.Pwd = NewHashPassword(resp.Salt, pwd, resp.Pepper)

	return resp
}

func NewHashPassword(salt string, pwd string, pepper string) (res string) {
	res = fmt.Sprintf("%s%s%s", salt, pwd, pepper)

	for i := 0; i < 2; i++ {
		sha_512 := sha512.New()
		sha_512.Write([]byte(res))
		res = base64.URLEncoding.EncodeToString(sha_512.Sum(nil))
	}

	return res
}
