package utils

import (
	"github.com/hyperjumptech/beda"
	"strings"
)

func ValidationName(req string, ktp string) bool {
	req = strings.ToLower(req)
	ktp = strings.ToLower(ktp)
	sd := beda.NewStringDiff(req, ktp)
	lDist := sd.LevenshteinDistance()

	if lDist <= 3 {
		return true
	}

	//spell checker
	req = strings.ReplaceAll(req, "mochamad", "moch")
	ktp = strings.ReplaceAll(ktp, "mochamad", "moch")

	req = strings.ReplaceAll(req, "mochammad", "moch")
	ktp = strings.ReplaceAll(ktp, "mochammad", "moch")

	req = strings.ReplaceAll(req, "muhammad", "muh")
	ktp = strings.ReplaceAll(ktp, "muhammad", "muh")

	req = strings.ReplaceAll(req, "muhammad", "muhamad")
	ktp = strings.ReplaceAll(ktp, "muhammad", "muhamad")

	req = strings.ReplaceAll(req, "muhamad", "moch")
	ktp = strings.ReplaceAll(ktp, "muhamad", "moch")

	req = strings.ReplaceAll(req, "moch", "muh")
	ktp = strings.ReplaceAll(ktp, "moch", "muh")

	req = strings.ReplaceAll(req, "made", "md")
	ktp = strings.ReplaceAll(ktp, "made", "md")

	finds := ""
	if len(req) < len(ktp) {
		finds = req
		req = ktp
	} else {
		finds = ktp
	}

	req = strings.ReplaceAll(req, " ", "")
	finds = strings.ReplaceAll(finds, " ", "")

	res := strings.Contains(req, finds)

	return res
}
