package utils

import "strings"

func GenerateCodeAnchorName(anchorName string) string {
	code := anchorName
	if len(anchorName) >= 3 {
		firstTwoChar := anchorName[:2]
		lastChar := anchorName[len(anchorName)-1:]
		code = firstTwoChar + lastChar
	}
	return strings.ToUpper(code)
}
