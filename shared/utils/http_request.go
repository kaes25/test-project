package utils

import (
	"bytes"
	"crypto/tls"
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"time"

	"github.com/go-resty/resty/v2"
)

type UtilHttpRequest struct {
	GET                               func(params Params) (out map[string]interface{}, err error)
	POST                              func(params Params) (out map[string]interface{}, err error)
	PUT                               func(params Params) (out map[string]interface{}, err error)
	POSTWithoutBasicAuth              func(params Params) (out map[string]interface{}, err error)
	GETWithoutBasicAuthSingleResponse func(params Params) (out string, err error)
	PostFormData                      func(params Params) (out map[string]interface{}, err error)
}

func NewUtilHttpRequest() *UtilHttpRequest {
	return &UtilHttpRequest{
		GET:                  GET,
		POST:                 POST,
		PUT:                  PUT,
		POSTWithoutBasicAuth: POSTWithoutBasicAuth,
	}
}

type Params struct {
	Url               string
	BasicAuthUsername string
	BasicAuthPassword string
	RequestBody       map[string]interface{}
	QueryParams       map[string]string
	RequestHeader     map[string]string
}

func NewParams(url string, basicAuthUsername string, basicAuthPassword string, requestBody map[string]interface{},
	queryParams map[string]string, requestHeader map[string]string) (out Params) {
	out.Url = url
	out.BasicAuthUsername = basicAuthUsername
	out.BasicAuthPassword = basicAuthPassword
	out.RequestBody = requestBody
	out.QueryParams = queryParams
	out.RequestHeader = requestHeader
	return out
}

type Params2 struct {
	Url               string
	BasicAuthUsername string
	BasicAuthPassword string
	RequestBody       map[string]interface{}
	QueryParams       map[string]string
	RequestHeader     []map[string]interface{}
}

func NewParams2(url string, basicAuthUsername string, basicAuthPassword string, requestBody map[string]interface{},
	queryParams map[string]string, requestHeader []map[string]interface{}) (out Params2) {
	out.Url = url
	out.BasicAuthUsername = basicAuthUsername
	out.BasicAuthPassword = basicAuthPassword
	out.RequestBody = requestBody
	out.QueryParams = queryParams
	out.RequestHeader = requestHeader
	return out
}

func POST(params Params) (out map[string]interface{}, err error) {
	client := resty.New()
	client.SetTimeout(time.Second * 300)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetDebug(true)
	payload, err := json.Marshal(params.RequestBody)
	if err != nil {
		return
	}
	host := fmt.Sprint(params.Url)
	resp, err := client.R().
		SetHeaders(params.RequestHeader).
		SetBasicAuth(params.BasicAuthUsername, params.BasicAuthPassword).
		SetBody(bytes.NewBuffer(payload)).
		SetHeaders(params.RequestHeader).
		Post(host)
	if err != nil {
		return
	}

	_ = json.Unmarshal(resp.Body(), &out)

	if resp.StatusCode() > http.StatusOK {
		return out, errors.New("")
	}

	return
}

func POSTNotification(params Params) (out *http.Response, err error) {
	//payload, err := json.Marshal(params.RequestBody)
	//if err != nil {
	//	return
	//}
	//
	//rBody := bytes.NewReader(payload)
	//req, err := http.NewRequest(http.MethodPost, params.Url, rBody)
	//ctx, cancel := context.WithTimeout(req.Context(), config.Notification.Timeout)
	//defer cancel()
	//req = req.WithContext(ctx)
	//req.Header.Set("Content-Type", "application/json")
	//if err != nil {
	//	return
	//}
	//req.SetBasicAuth(params.BasicAuthUsername, params.BasicAuthPassword)
	//client := http.Client{
	//	Timeout: time.Second * 5,
	//}
	//out, err = client.Do(req)
	client := resty.New()
	client.SetTimeout(time.Second * 5)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetDebug(true)
	payload, err := json.Marshal(params.RequestBody)
	if err != nil {
		return
	}

	host := fmt.Sprint(params.Url)
	resp, err := client.R().
		SetHeaders(params.RequestHeader).
		SetBasicAuth(params.BasicAuthUsername, params.BasicAuthPassword).
		SetBody(bytes.NewBuffer(payload)).
		Post(host)
	if err != nil {
		return
	}

	_ = json.Unmarshal(resp.Body(), &out)

	if resp.StatusCode() > http.StatusOK {
		return out, errors.New("")
	}

	return
}

func GET(params Params) (out map[string]interface{}, err error) {
	client := resty.New()
	client.SetTimeout(time.Second * 5)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetDebug(true)
	host := fmt.Sprint(params.Url)
	resp, err := client.R().
		SetQueryParams(params.QueryParams).
		SetHeaders(params.RequestHeader).
		SetBasicAuth(params.BasicAuthUsername, params.BasicAuthPassword).
		Get(host)
	if err != nil {
		return
	}

	_ = json.Unmarshal(resp.Body(), &out)

	if resp.StatusCode() > http.StatusOK {
		return out, errors.New("")
	}
	return
}

func GET2(params Params2) (out map[string]interface{}, err error) {
	client := resty.New()
	client.SetTimeout(time.Second * 5)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetDebug(true)
	host := fmt.Sprint(params.Url)
	resp, err := client.R().
		SetQueryParams(params.QueryParams).
		SetBasicAuth(params.BasicAuthUsername, params.BasicAuthPassword).
		Get(host)
	if err != nil {
		return
	}

	_ = json.Unmarshal(resp.Body(), &out)

	if resp.StatusCode() > http.StatusOK {
		return out, errors.New("")
	}
	return
}

func PUT(params Params) (out map[string]interface{}, err error) {
	client := resty.New()
	client.SetTimeout(time.Second * 5)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetDebug(true)
	payload, err := json.Marshal(params.RequestBody)
	if err != nil {
		return
	}
	host := fmt.Sprint(params.Url)
	resp, err := client.R().
		SetHeaders(params.RequestHeader).
		SetBasicAuth(params.BasicAuthUsername, params.BasicAuthPassword).
		SetQueryParams(params.QueryParams).
		SetBody(bytes.NewBuffer(payload)).
		Put(host)
	if err != nil {
		return
	}

	_ = json.Unmarshal(resp.Body(), &out)

	if resp.StatusCode() > http.StatusOK {
		return out, errors.New("")
	}

	return
}

func POSTWithoutBasicAuth(params Params) (out map[string]interface{}, err error) {
	client := resty.New()
	client.SetTimeout(time.Second * 30)
	client.SetTLSClientConfig(&tls.Config{InsecureSkipVerify: true})
	client.SetDebug(true)
	payload, _ := json.Marshal(params.RequestBody)
	host := fmt.Sprint(params.Url)

	resp, err := client.R().
		SetBody(bytes.NewBuffer(payload)).
		SetHeaders(params.RequestHeader).
		Post(host)
	if err != nil {
		return
	}

	_ = json.Unmarshal(resp.Body(), &out)
	if resp.StatusCode() >= http.StatusBadRequest {
		return out, errors.New("")
	}
	return
}
