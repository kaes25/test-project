package utils

import (
	"crypto/tls"
	"github.com/golang-jwt/jwt"
	"gitlab.com/kaes25/shared/config"
	"net/http"
	"time"
)

var c = &http.Client{
	//Disable SSL certificate Verification
	Transport: &http.Transport{
		TLSClientConfig: &tls.Config{
			InsecureSkipVerify: !config.App.HttpsEnabled,
		},
	},
}

func GenerateJWTToken(userId string) (tokens string, err error) {
	var sampleSecretKey = []byte("SecretYouShouldHide")

	token := jwt.New(jwt.SigningMethodHS256)
	claims := token.Claims.(jwt.MapClaims)
	claims["exp"] = time.Now().Add(10 * time.Minute)
	claims["authorized"] = true
	claims["user"] = userId

	tokens, err = token.SignedString(sampleSecretKey)
	if err != nil {
		return "", err
	}
	return
}
