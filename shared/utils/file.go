package utils

import (
	"errors"
	"io"
	"log"
	"mime/multipart"
	"os"
	"strconv"
	"strings"
)


func GenerateUniqueFilename(folderPath string, fileName string, counter int) (string, error) {
	if _, err := os.Stat(folderPath); os.IsNotExist(err) {
		errCreatingDir := os.MkdirAll(folderPath, 0777)
		if errCreatingDir != nil {
			log.Println("Unable to create directory. Permission denied")
			return "", errors.New("Unable to create directory. Permission denied")
		}
	}
	dotIndex := strings.LastIndex(fileName, ".")
	if dotIndex == -1 {
		log.Println("Invalid file name(no extension found)")
		return "", errors.New("Invalid file name(no extension found)")
	}
	fileNameWtExtn := fileName[0:dotIndex]
	extension := fileName[dotIndex:]
	fullFilePath := folderPath + string(os.PathSeparator) + fileNameWtExtn
	if counter != 0 {
		fullFilePath += "_" + strconv.Itoa(counter)
	}
	if _, err := os.Stat(fullFilePath + extension); os.IsNotExist(err) {
		return fullFilePath + extension, nil
	}
	counter += 1
	return GenerateUniqueFilename(folderPath, fileName, counter)
}

func SaveUploadedFile(file multipart.File, filePath string) error {
	f, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		return err
	}
	defer f.Close()
	_, errCopy := io.Copy(f, file)
	return errCopy
}

func CreateDirectory(path string) (bool, error) {
	if _, err := os.Stat(path); os.IsNotExist(err) {
		errCreatingDir := os.MkdirAll(path, 0777)
		if errCreatingDir != nil {
			log.Println("Unable to create directory. Permission denied")
			return false, errors.New("Unable to create directory. Permission denied")
		}
	}
	return true, nil
}

