build:
	@go build -o bin/main-linux main.go

run:
	@ENV_NAME=DEVELOPMENT go run main.go

mockgen:
	@mockgen --source=domain/article/repository.go --destination=mocks/domain/article/repository.go
#