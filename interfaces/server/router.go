package server

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"os"
)

func setupRouter(app *gin.Engine, handler *handler) {
	userService := app.Group("/test/api")

	// Working Directory
	wd, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println("@--------- Working Directory : ", wd)
	//app.LoadHTMLGlob(wd + "/shared/templates/*")

	setupAuth(userService, handler)
}

func setupAuth(app *gin.RouterGroup, handler *handler) {
	r := app.Group("/auth")
	v1 := r.Group("/v1")

	v1.GET("/article", handler.authHandler.GetArticle)
	v1.POST("/article", handler.authHandler.PostArticle)

}
