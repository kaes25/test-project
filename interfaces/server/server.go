package server

// start framework
import (
	"flag"
	"fmt"
	"gitlab.com/kaes25/interfaces/container"

	"github.com/gin-gonic/gin"
)

func StartApp(container container.Container) *gin.Engine {
	addr := flag.String("addr: ", container.Config.App.Addr, "Address to listen and serve")
	app := gin.Default()

	////Setup logger
	//log := logger.New(container.Config.Logger)
	//middleware.SetupMiddlewareLogger(app, log)

	//metaData := logger.MetaData{
	//	Service: container.Config.App.ServiceName,
	//}

	//Setup middleware logger - intercept incoming request + response to tdr.log
	//middleware_logger.SetupMiddlewareLogger(app, container.Logger, metaData)

	// Setup Handler
	handler := setupHandler(container)

	//Setup Router
	setupRouter(app, handler)

	fmt.Println(app.Run(*addr))
	return app
}
