package server

import "gitlab.com/kaes25/interfaces/container"

type handler struct {
	authHandler *authHandler
}

func setupHandler(container container.Container) *handler {
	authHandler := newAuthHandler(container.AuthService)
	return &handler{
		authHandler: authHandler,
	}
}
