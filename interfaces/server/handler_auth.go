package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/kaes25/shared/utils"
	"gitlab.com/kaes25/usecase/auth"
	"gitlab.com/kaes25/usecase/auth/request"
	"net/http"
)

type authHandler struct {
	service auth.Service
}

func newAuthHandler(service auth.Service) *authHandler {
	return &authHandler{service: service}
}

func (h *authHandler) GetArticle(ctx *gin.Context) {
	req, err := request.NewGetArticleRequest(ctx)
	if err != nil {
		utils.SendResponse(ctx, http.StatusBadRequest, false, err.Error(), nil)
		return
	}
	res, err := h.service.GetArticle(ctx, req)
	if err != nil {
		utils.SendResponse(ctx, http.StatusBadRequest, false, err.Error(), nil)
		return
	}

	utils.SendResponse(ctx, http.StatusOK, true, "Success", res)
}

func (h *authHandler) PostArticle(ctx *gin.Context) {
	req, err := request.NewPostArticleRequest(ctx)
	if err != nil {
		utils.SendResponse(ctx, http.StatusBadRequest, false, err.Error(), nil)
		return
	}
	res, err := h.service.PostArticle(ctx, req)
	if err != nil {
		utils.SendResponse(ctx, http.StatusBadRequest, false, err.Error(), nil)
		return
	}

	utils.SendResponse(ctx, http.StatusOK, true, "Success", res)
}
