package container

import (
	"gitlab.com/kaes25/infrastructure/mysql"
	"gitlab.com/kaes25/shared/config"
	"gitlab.com/kaes25/shared/database"
	"gitlab.com/kaes25/usecase/auth"
)

type Container struct {
	AuthService auth.Service
	Config      *config.Config
	//Logger      logger.Logger
}

func SetupContainer() (out Container) {
	//Setup config
	cfg := config.SetupConfig()

	// setup database connection
	db := database.SetupDatabase(cfg)

	//setup infra
	article := mysql.SetupArticleRepo(db)

	// setup service
	authService := auth.NewAuthService(cfg, article)

	return Container{
		AuthService: authService,
		Config:      cfg,
	}
}
