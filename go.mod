module gitlab.com/kaes25

go 1.13

require (
	github.com/gin-gonic/gin v1.7.7
	github.com/go-playground/validator/v10 v10.10.1
	github.com/go-resty/resty/v2 v2.4.0
	github.com/gofiber/fiber/v2 v2.40.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/google/uuid v1.3.0
	github.com/hyperjumptech/beda v1.0.0
	github.com/jellydator/ttlcache/v2 v2.11.1
	github.com/jinzhu/gorm v1.9.16
	github.com/spf13/viper v1.7.1
	golang.org/x/crypto v0.0.0-20220722155217-630584e8d5aa
	gorm.io/driver/postgres v1.4.5
	gorm.io/gorm v1.24.2

)
