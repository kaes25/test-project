package auth

import (
	"gitlab.com/kaes25/domain/article"
	"gitlab.com/kaes25/shared/config"
)

type service struct {
	cfg     *config.Config
	article article.Repository
}

func NewAuthService(cfg *config.Config,
	article article.Repository,
) Service {
	return &service{
		cfg:     cfg,
		article: article,
	}
}
