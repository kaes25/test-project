package auth

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"gitlab.com/kaes25/entity"
	"gitlab.com/kaes25/shared/constant"
	"gitlab.com/kaes25/usecase/auth/request"
)

func (s *service) PostArticle(ctx *gin.Context, in request.PostArticleRequest) (out string, err error) {
	var data entity.Article

	data.SecureId = uuid.NewString()
	data.Author = in.Author
	data.Title = in.Title
	data.Body = in.Body

	_, err = s.article.SaveOrUpdateArticle(data)
	if err != nil {
		return out, errors.New("something wrong when save or update data")
	}

	out = constant.SuccessMsg

	return
}
