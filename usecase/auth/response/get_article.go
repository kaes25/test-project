package response

import "time"

type GetArticleResponse struct {
	ID          string    `json:"id"`
	Author      string    `json:"author"`
	Title       string    `json:"title"`
	Body        string    `json:"body"`
	CreatedDate time.Time `json:"created_date"`
}
