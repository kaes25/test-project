package request

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"io/ioutil"
)

type PostArticleRequest struct {
	Title  string `json:"title" validate:"required"`
	Body   string `json:"body" validate:"required"`
	Author string `json:"author" validate:"required"`
}

func NewPostArticleRequest(in *gin.Context) (out PostArticleRequest, err error) {
	body, _ := ioutil.ReadAll(in.Request.Body)
	err = json.Unmarshal(body, &out)
	if err != nil {
		return
	}
	v := validator.New()
	err = v.Struct(out)

	return
}
