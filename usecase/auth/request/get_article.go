package request

import (
	"errors"
	"github.com/gin-gonic/gin"
)

type GetArticleRequest struct {
	ID string `json:"secure_id"`
}

func NewGetArticleRequest(in *gin.Context) (out GetArticleRequest, err error) {
	out.ID = in.Request.URL.Query().Get("secure_id")
	if out.ID == "" {
		return out, errors.New("ID cannot be null")
	}

	return
}
