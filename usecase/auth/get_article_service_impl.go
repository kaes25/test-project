package auth

import (
	"encoding/json"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/jellydator/ttlcache/v2"
	"gitlab.com/kaes25/entity"
	"gitlab.com/kaes25/usecase/auth/request"
	"gitlab.com/kaes25/usecase/auth/response"
	"time"
)

var cache ttlcache.SimpleCache = ttlcache.NewCache()

func (s *service) GetArticle(ctx *gin.Context, in request.GetArticleRequest) (out response.GetArticleResponse, err error) {

	//cache
	cache.SetTTL(time.Duration(100000000 * time.Second))

	data, _ := verifyCache(in.ID)
	if data.Id == 0 {
		data, err = s.article.GetArticleBySecureId(in.ID)
		if err != nil {
			return out, errors.New("something wrong when check data")
		}
		if data.Id == 0 {
			return out, errors.New("email not found")
		}
		cache.Set(in.ID, data)
	}

	out.ID = data.SecureId
	out.Author = data.Author
	out.Title = data.Title
	out.Body = data.Body
	out.CreatedDate = data.CreatedDate

	return
}

func verifyCache(secureId string) (result entity.Article, err error) {
	val, err := cache.Get(secureId)
	if err != nil {
		return result, err
	}

	data, _ := json.Marshal(val)
	err = json.Unmarshal(data, &result)
	if err != nil {
		return result, err
	}
	return
}
