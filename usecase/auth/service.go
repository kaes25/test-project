package auth

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/kaes25/usecase/auth/request"
	"gitlab.com/kaes25/usecase/auth/response"
)

type Service interface {
	GetArticle(ctx *gin.Context, in request.GetArticleRequest) (out response.GetArticleResponse, err error)
	PostArticle(ctx *gin.Context, in request.PostArticleRequest) (out string, err error)
}
