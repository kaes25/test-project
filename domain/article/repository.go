package article

import "gitlab.com/kaes25/entity"

type Repository interface {
	SaveOrUpdateArticle(in entity.Article) (out string, err error)
	GetArticleBySecureId(secureId string) (out entity.Article, err error)
}
