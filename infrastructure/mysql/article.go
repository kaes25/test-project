package mysql

import (
	"errors"
	"github.com/jinzhu/gorm"
	"gitlab.com/kaes25/domain/article"
	"gitlab.com/kaes25/entity"
	"gitlab.com/kaes25/shared/database"
)

type articleRepo struct {
	db *database.Database
}

func SetupArticleRepo(db *database.Database) article.Repository {
	return &articleRepo{db: db}
}

func (r *articleRepo) SaveOrUpdateArticle(in entity.Article) (out string, err error) {
	operation := r.db.GetDB().Save(&in)
	if operation.Error != nil {
		if err != nil {
			return out, errors.New("something wrong when save or update data")
		}
	}
	return out, operation.Error
}

func (r *articleRepo) GetArticleBySecureId(secureId string) (out entity.Article, err error) {
	err = r.db.GetDB().Where("secure_id = ?", secureId).Find(&out).Error
	if err != nil {
		if err == gorm.ErrRecordNotFound {
			return out, nil
		} else {
			return out, errors.New("something wrong when check data")
		}
	}
	return
}
