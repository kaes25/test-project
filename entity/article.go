package entity

import (
	"gitlab.com/kaes25/shared/constant"
	"time"
)

type Article struct {
	Id           uint64    `gorm:"column:id"`
	CreatedBy    string    `gorm:"column:created_by"`
	ModifiedBy   string    `gorm:"column:modified_by"`
	CreatedDate  time.Time `gorm:"column:created_date"`
	ModifiedDate time.Time `gorm:"column:modified_date"`
	SecureId     string    `gorm:"column:secure_id"`
	Author       string    `gorm:"column:author"`
	Title        string    `gorm:"column:title"`
	Body         string    `gorm:"column:body"`
}

func (u Article) TableName() string {
	return constant.EntityArticle
}

func (u Article) IsEmpty() bool { return u == Article{} }
