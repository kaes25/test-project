package main

import (
	"gitlab.com/kaes25/interfaces/container"
	"gitlab.com/kaes25/interfaces/server"
)

func main() {
	server.StartApp(container.SetupContainer())
}
